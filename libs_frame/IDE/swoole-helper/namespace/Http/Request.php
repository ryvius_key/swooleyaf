<?php
namespace Swoole\Http;

/**
 * @since 4.5.1
 */
class Request
{


    /**
     * @return mixed
     */
    public function rawContent(){}

    /**
     * @return mixed
     */
    public function getContent(){}

    /**
     * @return mixed
     */
    public function getData(){}

    /**
     * @return mixed
     */
    public function __destruct(){}


}
