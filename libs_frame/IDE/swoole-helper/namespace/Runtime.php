<?php
namespace Swoole;

/**
 * @since 4.5.1
 */
class Runtime
{


    /**
     * @param $enable[optional]
     * @param $flags[optional]
     * @return mixed
     */
    public static function enableCoroutine($enable=null, $flags=null){}

    /**
     * @return mixed
     */
    public static function getHookFlags(){}

    /**
     * @param $flags[required]
     * @return mixed
     */
    public static function setHookFlags($flags){}


}
